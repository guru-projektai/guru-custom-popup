<?php

class Guru_Custom_Popup_Widget extends WP_Widget
{
    const WIDGET_ID = 'guru_custom_popup_widget';
    const WIDGET_NAME = 'Guru custom popup widget';

    public function __construct()
    {
        $widget_ops = array(
            'classname' => self::WIDGET_ID,
            'description' => 'A widget guru custom popup',
        );

        parent::__construct(self::WIDGET_ID, self::WIDGET_NAME, $widget_ops);
    }

    public function widget($args, $instance)
    {
        if ($instance['activated'] === false ) return;

        $desktop_page = $instance['desktop_page'];
        if (! empty($desktop_page)){
            $desktop_page_id = $desktop_page;
            $desktop_page_url = get_permalink($desktop_page);
        }

        $mobile_page = $instance['mobile_page'];
        if (! empty($mobile_page)) {
            $mobile_page_id = $mobile_page;
            $mobile_page_url = get_permalink($mobile_page);
        }

        wp_register_script('popup-js', plugins_url('/assets/js/popup.js', Guru_Custom_Popup_FILE), array('jquery'));

        if (isset($desktop_page_url) && isset($mobile_page_url) ) {
            wp_localize_script('popup-js','guruPopup', array(
                'desktop_page' => [
                    'url' => $desktop_page_url,
                    'id' => $desktop_page_id
                ],
                'mobile_page' => [
                    'url' => $mobile_page_url,
                    'id' => $mobile_page_id
                ],
                'unique_name' => $instance['unique_name'],
                'start_date' => $instance['start_date'],
                'end_date' => $instance['end_date'],
                'cookie_expiration_hours' => $instance['cookie_expiration_hours'],
                'popup_width' => $instance['popup_width'],
                'hide_from_uk_visitors' => $instance['hide_from_uk_visitors'],
            ));
        }

        wp_enqueue_script('popup-js');
    }

    public function form($instance)
    {
        wp_enqueue_style('flatpickr-datetimepicker-css', '//cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.css');
        wp_enqueue_script('flatpickr-datetimepicker-js', '//cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.js');

        $week_plus = gmdate("Y-m-d\TH:i:s\Z", time() + 3600 * 24 * 7);

        $unique_name = ! empty($instance['unique_name']) ? $instance['unique_name'] : 'unique-popup-name-dashes';
        $activated = ! empty($instance['activated']) ? checked($instance['activated'], 'on', false) : false;
        $hide_from_uk_visitors = ! empty($instance['hide_from_uk_visitors']) ? checked($instance['hide_from_uk_visitors'], 'on', false) : false;
        $desktop_page = ! empty($instance['desktop_page']) ? $instance['desktop_page'] : '0';
        $mobile_page = ! empty($instance['mobile_page']) ? $instance['mobile_page'] : '0';
        $start_date = ! empty($instance['start_date']) ? $instance['start_date'] : gmdate("Y-m-d\TH:i:s\Z");
        $end_date = ! empty($instance['end_date']) ? $instance['end_date'] : $week_plus;
        $popup_width = ! empty($instance['popup_width']) ? $instance['popup_width'] : 100;
        $cookie_expiration_hours = ! empty($instance['cookie_expiration_hours']) ? $instance['cookie_expiration_hours'] : 6;

        include Guru_Custom_Popup_PATH . 'includes/widget/guru-custom-popup-widget-form.php';
    }

    public function update($new_instance, $old_instance)
    {
        $instance = [];

        $instance['unique_name'] = (! empty($new_instance['unique_name'])) ? $new_instance['unique_name'] : 'unique-popup-name-dashes';
        $instance['activated'] = (! empty($new_instance['activated'])) ? $new_instance['activated'] : false;
        $instance['hide_from_uk_visitors'] = (! empty($new_instance['hide_from_uk_visitors'])) ? $new_instance['hide_from_uk_visitors'] : false;
        $instance['desktop_page'] = (! empty($new_instance['desktop_page'])) ? $new_instance['desktop_page'] : '0';
        $instance['mobile_page'] = (! empty($new_instance['mobile_page'])) ? $new_instance['mobile_page'] : '0';
        $instance['start_date'] = (! empty($new_instance['start_date'])) ? $new_instance['start_date'] : null;
        $instance['end_date'] = (! empty($new_instance['end_date'])) ? $new_instance['end_date'] : null;
        $instance['popup_width'] = (! empty($new_instance['popup_width'])) ? $new_instance['popup_width'] : '100';
        $instance['cookie_expiration_hours'] = (! empty($new_instance['cookie_expiration_hours'])) ? $new_instance['cookie_expiration_hours'] : 6;

        return $instance;
    }
}

function guru_custom_popup_widget()
{
    register_sidebar(
        [
            'name' => Guru_Custom_Popup_Widget::WIDGET_NAME,
            'id' => Guru_Custom_Popup_Widget::WIDGET_ID,
            'description' => Guru_Custom_Popup_Widget::WIDGET_NAME,
            'before_widget' => '<div class="guru-custom-popup">',
            'after_widget' => '</div>',
        ]
    );
}

add_action('widgets_init', Guru_Custom_Popup_Widget::WIDGET_ID);

add_action('widgets_init', function () {
    register_widget('Guru_Custom_Popup_Widget');
});
