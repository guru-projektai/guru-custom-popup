<p class="guru-custom-popup-options">
    <label for="<?= esc_attr($this->get_field_id('activated')) ?>">
        <?php esc_attr_e( 'Activated:', 'text_domain' ); ?>
    </label>
    <input type="checkbox"
           class="checkbox"
           <?= $activated ?>
           id="<?= esc_attr($this->get_field_id('activated')) ?>"
           name="<?= esc_attr($this->get_field_name('activated')) ?>"
    />

    <br />

    <label for="<?= esc_attr($this->get_field_id('hide_from_uk_visitors')) ?>">
        <?php esc_attr_e( 'Hide from UK visitors:', 'text_domain' ); ?>
    </label>
    <input type="checkbox"
           class="checkbox"
           <?= $hide_from_uk_visitors ?>
           id="<?= esc_attr($this->get_field_id('hide_from_uk_visitors')) ?>"
           name="<?= esc_attr($this->get_field_name('hide_from_uk_visitors')) ?>"
    />

    <br />

    <label for="<?= esc_attr($this->get_field_id('unique_name')) ?>">
        <?php esc_attr_e( 'Unique name (dash seperated)*', 'text_domain' ); ?>
    </label>
    <input type="text"
           id="<?= esc_attr($this->get_field_id('unique_name')) ?>"
           name="<?= esc_attr($this->get_field_name('unique_name')) ?>"
           value="<?= esc_attr($unique_name) ?>"
    />

    <br />

    <label for="<?= esc_attr($this->get_field_id('desktop_page')) ?>">
        <?php esc_attr_e( 'Desktop Popup page:', 'text_domain' ); ?>
    </label>
    <?php wp_dropdown_pages( array(
            'name' => $this->get_field_name('desktop_page'),
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-custom-popup.php',
            'show_option_none' => __( '— Select —' ),
            'option_none_value' => '0',
            'selected' => $desktop_page,
        )
    ); ?>

    <br />

    <label for="<?= esc_attr($this->get_field_id('mobile_page')) ?>">
        <?php esc_attr_e( 'Mobile Popup page:', 'text_domain' ); ?>
    </label>
    <?php wp_dropdown_pages( array(
            'name' => $this->get_field_name('mobile_page'),
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-custom-popup.php',
            'show_option_none' => __( '— Select —' ),
            'option_none_value' => '0',
            'selected' => $mobile_page,
        )
    ); ?>

    <br />

    <div class="input-date-start">
        <label for="<?= esc_attr($this->get_field_id('start_date')) ?>">
            <?php esc_attr_e( 'Start date:', 'text_domain' ); ?>
        </label>
        <input type="text"
               class="start_date date-flatpickr"
               placeholder="Please select Date Time"
               data-input
               id="<?= esc_attr($this->get_field_id('start_date')) ?>"
               name="<?= esc_attr($this->get_field_name('start_date')) ?>"
               data-default-date="<?= $start_date ?>"
               value="<?= $start_date ?>"
        />
    </div>

    <br />

    <div class="input-date-end">
        <label for="<?= esc_attr($this->get_field_id('end_date')) ?>">
            <?php esc_attr_e( 'End date:', 'text_domain' ); ?>
        </label>
        <input type="text"
               class="end_date date-flatpickr"
               placeholder="Please select Date Time"
               data-input
               id="<?= esc_attr($this->get_field_id('end_date')) ?>"
               name="<?= esc_attr($this->get_field_name('end_date')) ?>"
               data-default-date="<?= $end_date ?>"
               value="<?= $end_date ?>"
        />
    </div>

    <br />

    <label for="<?= esc_attr($this->get_field_id('popup_width')) ?>">
        <?php esc_attr_e( 'Popup width (%):', 'text_domain' ); ?>
    </label>
    <input type="text"
           id="<?= esc_attr($this->get_field_id('popup_width')) ?>"
           name="<?= esc_attr($this->get_field_name('popup_width')) ?>"
           value="<?= esc_attr($popup_width) ?>"
    />

    <br />

    <label for="<?= esc_attr($this->get_field_id('cookie_expiration_hours')) ?>">
        <?php esc_attr_e( 'Cookie expiration (hours)', 'text_domain' ); ?>
    </label>
    <input type="number"
           id="<?= esc_attr($this->get_field_id('cookie_expiration_hours')) ?>"
           name="<?= esc_attr($this->get_field_name('cookie_expiration_hours')) ?>"
           value="<?= esc_attr($cookie_expiration_hours) ?>"
    />
</p>

<script type="text/javascript">
  jQuery(document).ready(function($) {
    let timeFieldOne = document.querySelectorAll(".start_date");
    flatpickr(timeFieldOne[timeFieldOne.length-1], {
      enableTime: true,
      dateFormat: "Z",
      altInput: true,
      altFormat: "Y-m-d h:i K",
      time_24hr: true,
      onChange: function (date, dateStr) {
        $('.start_date').val(dateStr)
      },
      onReady: function() {
        $('.start_date').data('default-date', '<?= $start_date ?>')
        $('.start_date').val('<?= $start_date ?>')
      }
    });

    let timeFieldTwo = document.querySelectorAll(".end_date");
    flatpickr(timeFieldTwo[timeFieldTwo.length-1], {
      enableTime: true,
      dateFormat: "Z",
      altInput: true,
      altFormat: "Y-m-d h:i K",
      time_24hr: true,
      onChange: function (date, dateStr) {
        $('.end_date').val(dateStr)
      },
      onReady: function() {
        $('.end_date').data('default-date', '<?= $end_date ?>')
        $('.end_date').val('<?= $end_date ?>')
      }
    });

  })
</script>
