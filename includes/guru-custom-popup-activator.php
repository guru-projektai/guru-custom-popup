<?php

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 */
class Guru_Custom_Popup_Activator
{
    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     */
    public static function activate()
    {
        if (current_user_can('activate_plugins') && !class_exists('WP_Post_Modal')) {
            activate_plugin('wp-post-modal/wp-post-modal.php');
        }
    }
}
