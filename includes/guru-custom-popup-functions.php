<?php

/**
 * Retrieves plugin options
 *
 * @param null $variable
 * @param null $default
 * @return mixed|void
 */
function get_guru_custom_popup_option($variable = null, $default = null) {
    $options = get_option(Guru_Custom_Popup_Constants::OPTIONS);

    if ($options && $variable) {
        $options = $options[$variable];

        if ($options == '' || ! $options) {
            return $default;
        }
    }

    return $options;
}