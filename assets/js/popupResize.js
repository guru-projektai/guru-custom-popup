var modalHidden = true
var frameLoaded = false

function popupResize(open = true) {
  let iframe = jQuery('#iframe-modal')

  if (iframe[0]) {
    setTimeout(function() {
      iframe.height(iframe[0].contentWindow.document.body.clientHeight + 1)
    }, 0)
  }

  if (! open) return

  frameLoaded = true
  modalHidden = false

  if (! modalHidden) jQuery('html').addClass('non-scroll')

  jQuery('.modal-wrapper').removeClass('hideItPlease')
}

setInterval(function() {
  if (modalHidden && frameLoaded) {
    showHiddenModal()
  }
}, 1000)

function showHiddenModal() {
  if (frameLoaded && modalHidden) {
    popupResize()
  }
}

let rTime
let timeout = false
let delta = 200

// On window resize with debounce
jQuery(window).on('resize', function () {
  if (window.location.href.indexOf('popupas') > -1) return

  rTime = new Date()

  if (timeout === false) {
    timeout = true
    setTimeout(resizeEnd, delta)
  }
})

function resizeEnd() {
  if (new Date() - rTime < delta) {
    setTimeout(resizeEnd, delta)
  } else {
    timeout = false

    popupResize(false)
  }
}
