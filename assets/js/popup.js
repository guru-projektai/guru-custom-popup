jQuery(document).ready(function($) {
  class Page {
    getWindowWidth() {
      return $(window).width()
    }

    getUrl() {
      return window.location.href
    }

    setCookie(name, value, days) {
      let expires = ""
      if (days) {
        let date = new Date()
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000))
        expires = "; expires=" + date.toUTCString()
      }

      document.cookie = name + "=" + (value || "")  + expires + "; path=/"
    }

    getCookie(name) {
      const nameEQ = name + "="
      const ca = document.cookie.split(';')
      for (let i = 0; i < ca.length; i++) {
        let c = ca[i]
        while (c.charAt(0) === ' ') c = c.substring(1, c.length)
        if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length)
      }

      return null
    }

    getUrlParameter(sParam) {
      let sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i

      for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=')

        if (sParameterName[0] === sParam) {
          return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1])
        }
      }
    }

    isUserFromUK() {
      return $('body').hasClass('from-uk')
    }
  }

  class Popup extends Page {
    popupPageUniqueName = ''

    constants = {
      DESKTOP: 'desktop',
      MOBILE: 'mobile',
      TABLET_WIDTH: 992,
    }

    access() {
      let access = true

      // Do not load if page is popup itself (prevent JS load duplication on iframe loaded page)
      if ((typeof guruPopup === 'undefined') || this.isPopupPage()) access = false

      // Do not load if visitor is from UK
      if (guruPopup.hide_from_uk_visitors === 'on') if (this.isUserFromUK()) access = false

      return access
    }

    loadPageType() {
      return this.getWindowWidth() > this.constants.TABLET_WIDTH ?
        this.constants.DESKTOP :
        this.constants.MOBILE
    }

    initialize() {
      if (! this.access()) return

      this.appendModal(this.loadPageType())
    }

    onResizeWindow() {
      $(window).on('resize', (window) => {
        const modal = $('.modal-wrapper .modal')
        modal.css('width', '' + this.returnWidth() + '%')

        if (window.target.innerWidth < this.constants.TABLET_WIDTH)
          modal.css('width', '95%')

      })
    }

    isPopupPage() {
      return this.getUrl().indexOf('popupas') > -1
    }

    appendModal(modalType = this.constants.DESKTOP) {
      this.popupPageUniqueName = 'popup-page-' + guruPopup.unique_name

      // Check if time popup is activated between selected time
      // Check if page is not popup itself to prevent double JS loading
      // Check if modal_preview is set for testing
      if ((! this.popupTimeShow() || this.getCookie(this.popupPageUniqueName)) && typeof this.getUrlParameter('modal_preview') === 'undefined') {
        return
      }

      let appendPageUrl = ''
      switch (modalType) {
        case this.constants.MOBILE:
          appendPageUrl = guruPopup.mobile_page.url
          break
        case this.constants.DESKTOP:
          appendPageUrl = guruPopup.desktop_page.url
          break
      }

      $('header').append('<a class="modal-link popup-link-page-'+ this.popupPageUniqueName +' iframe hide" href="' + appendPageUrl + '" data-div="post-contents"></a>')

      this.onResizeWindow()

      this.setCookie(this.popupPageUniqueName, true, guruPopup.cookie_expiration_hours / 24)

      this.openModal()
    }

    popupTimeShow() {
      const currentTime = new Date().getTime()
      const popupStartDateTime = new Date(guruPopup.start_date).getTime()
      const popupEndDateTime = new Date(guruPopup.end_date).getTime()

      return ((popupStartDateTime <= currentTime) && (popupEndDateTime >= currentTime))
    }

    openModal() {
      $('.modal-wrapper').addClass('hideItPlease')
      $('.popup-link-page-' + this.popupPageUniqueName).click()
    }

    returnWidth() {
      return this.getWindowWidth() > this.constants.TABLET_WIDTH ? guruPopup.popup_width : ''
    }
  }

  (new Popup()).initialize()

})
