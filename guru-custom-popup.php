<?php
/**
 * Plugin Name: Guru Custom Popup
 * Description: Guru Custom Popup
 * Version: 1.0.0
 * Author: Mindaugas
 */

if ( !defined( 'ABSPATH' ) ) {
    die;
}

define( 'Guru_Custom_Popup_FILE', __FILE__ );
define( 'Guru_Custom_Popup_PATH', plugin_dir_path( Guru_Custom_Popup_FILE ) );
define( 'Guru_Custom_Popup_URL', plugin_dir_url( Guru_Custom_Popup_FILE ) );

register_deactivation_hook( Guru_Custom_Popup_FILE, array( 'Guru_Custom_Popup', 'guru_custom_popup_deactivate' ) );

final class Guru_Custom_Popup {

    /**
     * Plugin instance.
     *
     * @var Guru_Custom_Popup
     * @access private
     */
    private static $instance = null;

    /**
     * Get plugin instance.
     *
     * @return Guru_Custom_Popup
     * @static
     */
    public static function get_instance() {
        if ( ! isset(self::$instance) ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Constructor.
     *
     * @access private
     */
    private function __construct() {
        register_activation_hook(Guru_Custom_Popup_FILE, array($this , 'guru_custom_popup_activate'));

        add_action('admin_init', array($this, 'plugin_dependencies'));

        $this->guru_custom_popup_includes();

        add_action('wp_enqueue_scripts', array($this, 'enqueue'));

        add_action('admin_enqueue_scripts', array($this, 'load_custom_wp_admin_style'));

        add_action('activated_plugin', array($this, 'save_error'));
    }

    /**
     * Deactivate plugin if WP_Post_Modal is deactivated
     */
    public function plugin_dependencies()
    {
        if (! class_exists('WP_Post_Modal')) deactivate_plugins(plugin_basename(__FILE__));
    }

    public function load_custom_wp_admin_style()
    {
        wp_register_style('guru-custom-popup-widget-admin-css', plugins_url( '/assets/dist/admin.css', __FILE__));
        wp_enqueue_style('guru-custom-popup-widget-admin-css');
    }

    /**
     * Enqueue styles and js
     */
    public function enqueue()
    {
        wp_enqueue_script('popup-resize', plugins_url( '/assets/js/popupResize.js', __FILE__), array('jquery'));
        wp_enqueue_style('popup-resize-css', plugins_url( '/assets/dist/popup.css', __FILE__));
    }

    /**
     * Saves error to txt file (in case)
     */
    public function save_error()
    {
        file_put_contents(dirname(__file__). '/error_activation.txt', ob_get_contents());
    }

    /**
     * Run when deactivate plugin.
     *
     * Deletes pods database
     * Deletes plugin options
     * Deletes scheduled hooks
     */
    public static function guru_custom_popup_deactivate() {
        require_once Guru_Custom_Popup_PATH . 'includes/guru-custom-popup-deactivator.php';
        Guru_Custom_Popup_Deactivator::deactivate();
    }

    /**
     * Run when activate plugin.
     *
     * Runs migration (database)
     * Imports default settings (API URL, API key, etc...)
     * Fetches from API first time to have data
     */
    public function guru_custom_popup_activate() {
        require_once Guru_Custom_Popup_PATH . 'includes/guru-custom-popup-activator.php';
        Guru_Custom_Popup_Activator::activate();

    }

    /**
     * Loading plugin functions files
     */
    public function guru_custom_popup_includes() {
        require_once __DIR__ . '/includes/guru-custom-popup-functions.php';
        require_once __DIR__ . '/includes/widget/guru-custom-popup-widget.php';

//        require_once __DIR__ . '/includes/admin-page.php';
    }

}

function Guru_Custom_Popup() {
    return Guru_Custom_Popup::get_instance();
}

$GLOBALS['Guru_Custom_Popup'] = Guru_Custom_Popup();
